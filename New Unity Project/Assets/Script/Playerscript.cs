﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.InputSystem;
using TMPro;
using System.Runtime.InteropServices;
using System.Diagnostics;
using UnityEngine.SceneManagement;
using System.Security.Cryptography;
using System.Collections.Specialized;

public class Playerscript : MonoBehaviour
{
    public float speed = 0; // starting speed float
    public TextMeshProUGUI countText; //count text
    public TextMeshProUGUI HealthText; // Health text
    public GameObject winTextObject;// Gameover text 
    private Rigidbody rb;
    private int count; // starting count
    private float movementX;
    private float movementY;
    private int health; // starting health
    public float jumppower;// force of jump
    bool onGround = false;//wheter ball is on ground or not

    // Start is called before the first frame update
    void Start()
    {
        rb = GetComponent<Rigidbody>();
        count = 0; // starting count
        health = 3; // starting health
        SetCountText(); // set count to text
        winTextObject.SetActive(false);
        SetHealthText(); // set health to text
    }
    private void Update()
    { onGround = Physics.Raycast(transform.position, Vector3.down, .51f);//racast to see if it collides with another object .51f away for jump mechanics

        //speed boost
        if (Keyboard.current.rKey.isPressed) // if the r key is pressed down
        {
            speed = 50f; //speed boost speed
        }
        else
            speed = 25f; // reagular speed                
    }

    private void OnMove(InputValue movementValue) // movement 
    {
        Vector2 movementVector = movementValue.Get<Vector2>();

        movementX = movementVector.x;
        movementY = movementVector.y;
    }
    private void FixedUpdate()
    {
        Vector3 movement = new Vector3(movementX, 0.0f, movementY);

        rb.AddForce(movement * speed); // add force * speed
    }
    void OnJump()
    {
       Jump(); //jump input
    }
    void Jump()
    {
        if (onGround) // if player is on the ground 
            rb.AddForce(Vector3.up * jumppower); // jump code player go up * force of jumpower
    }

    //Pickup Collsion Code
    private void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.CompareTag("Pickup")) //if player hits a pickup
        {
            other.gameObject.SetActive(false); 
            count = count + 1; // count increase by one

            SetCountText();// update count text
        }
        
    }
    private void OnCollisionEnter(Collision collision)
    {    if (collision.gameObject.CompareTag("Hazzard Pickup")) // hit hazzard pickup
         {
           
            health--;            //health goes down by one
            SetHealthText();
            if (health <= 0)       //if health is 0 restart scene
            {
                Gameover(); //game restarts
            }
         }
    }
     void Gameover()
     { 
            SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex);  //Reload Starting Scene
           

     }
    // Count Score tracker and win game text
    void SetCountText()
    {
        countText.text = "Count: " + count.ToString(); // add   score to canvas

        if (count >= 5) // if count less or equal to 5
            {
            winTextObject.SetActive(true); //game win text appears         
            }
    }
    void SetHealthText()
    {
        HealthText.text = "health: " + health.ToString(); // add   health to canvas

    }
}